// 1 Write the application code, it should expose prometheus metrics to answer these questions:

// How many validators form the active set?
// If a concrete validator (from a provided list) didn't produce any blocks 
// in the last 40 minutes (since the beginning of the current session), 
// has it sent a heartbeat? Think also about when this "alert" should eventually resolve.
// Was one concrete validator (from a provided list) reported offline in the last session?

// 2 Create a Helm chart for deploying it. 
// Add as many resources as you think could be necessary for 
// using it with prometheus-operator (https://github.com/coreos/prometheus-operator). 
// Involve also the AlertManager and define some meaningful alerts.

// 3 We want to make the metrics available to an external prometheus instance through a WireGuard VPN 
// (https://www.wireguard.com/). 
// Assume that the cluster's worker nodes run on ubuntu LTS and the VPN peer is on IPv4 a.b.c.d and its public key is provided.

//const sessionIndex = await api.query.session.currentIndex()
//console.log(sessionIndex)
//const authoredBlocks = await api.query.imOnline.authoredBlocks(sessionIndex, validators[0])
//console.log(authoredBlocks)
//const imOnline = await api.query.imOnline.receivedHeartbeats(sessionIndex, authoredBlocks[])
//console.log(imOnline)
// validators.forEach((validator, index) => {
//  const validatorId = validator.toString()
//  console.log(validatorId)
// })
//const heartBeats = await api.query.imOnline.receivedHeartbeats.multi(validators.map((_address, index) => [sessionIndex, index]))
//console.log(heartBeats[0])

const express                    = require('express')
const { ApiPromise, WsProvider } = require('@polkadot/api')

const app = express()

app.listen(8080, () => {
  console.log('listening for request on port 8080')
})

app.get('/', (req, res) => {
  (async () => {
    const provider   = new WsProvider('wss://kusama-rpc.polkadot.io')
    const api        = await ApiPromise.create({ provider })
    const validators = (await api.query.session.validators()).length

    return validators
  })()
    .then((result) => res.send(result.toString()))
    .catch(console.error)
})
