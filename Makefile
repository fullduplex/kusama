
destroy: destroy-cluster

destroy-and-uninstall-application:
	- helm uninstall kusama

destroy-cluster:
	- kind delete cluster

destroy-prometheus:
	- helm uninstall prometheus

destroy-wireguard:
	- kubectl delete -f resources/wireguard.yaml

install: install-cluster install-prometheus install-wireguard install-application

install-application:
	- docker build -t localhost:5000/kusama:latest -f ./docker/Dockerfile .
	- docker push localhost:5000/kusama:latest
	- helm upgrade --install kusama helm/kusama

install-cluster:
	- chmod +x resources/init-cluster-and-registry.sh
	- resources/init-cluster-and-registry.sh

install-prometheus:
	- helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
	- helm install prometheus prometheus-community/prometheus 

install-wireguard:
	- sudo add-apt-repository ppa:wireguard/wireguard
	- sudo apt install wireguard
	- kubectl apply -f resources/wireguard.yaml
